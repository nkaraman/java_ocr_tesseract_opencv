package com.hareg.master.thesis.AmharicOCR.controller;

import com.hareg.master.thesis.AmharicOCR.entity.ScanResult;
import com.hareg.master.thesis.AmharicOCR.services.RectContour;
import com.hareg.master.thesis.AmharicOCR.services.ScanProcessing;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hareg.master.thesis.AmharicOCR.entity.Property;
import com.hareg.master.thesis.AmharicOCR.repository.ImageRepository;
import com.hareg.master.thesis.AmharicOCR.services.ImageProcessing;

import nu.pattern.OpenCV;

@BasePathAwareController
public class ImageUploadController {



    private final ImageRepository imageRepository;
    
    public ImageUploadController(final ImageRepository imageRepository) {
		this.imageRepository = imageRepository;
		
    }

	@RequestMapping(path="/image/upload", method=RequestMethod.POST)
	@ResponseBody
	public List<Property> uploadImage(@RequestParam("file") MultipartFile file) throws IOException {

		final File tempFile = File.createTempFile("tmp", "jpg", null);
		Files.write(tempFile.toPath(), file.getBytes());

		
		OpenCV.loadShared();

		return ImageProcessing.processImage(tempFile.getAbsolutePath());

	}

	@RequestMapping(path="/image/process/internal", method=RequestMethod.POST)
	@ResponseBody
	public List<Property> uploadImage() throws IOException {
		OpenCV.loadShared();

		return ImageProcessing.processImage(ImageUploadController.class.getClassLoader().getResource("photos/new.jpg").getPath());

	}

	@RequestMapping(path="/image/process", method=RequestMethod.POST)
	@ResponseBody
	public List<RectContour> uploadImage(@RequestParam(name = "fileName") String fileName) throws Exception {


		OpenCV.loadShared();

		final String path = "witcher/" + fileName;
		final URL resource = ImageUploadController.class.getClassLoader().getResource(path);
		final String absolutPath = resource.getPath();
		return ScanProcessing.processImage(absolutPath);

	}
}